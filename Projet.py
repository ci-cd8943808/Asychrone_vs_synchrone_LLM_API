from transformers import GPT2LMHeadModel, GPT2Tokenizer
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException,Request
import uvicorn

class Model(BaseModel):
    model_name: str
    model_nb_tokens: int
    model_temperature: float

async def process(prompt: str, model: Model):
    try:
        tokenizer = GPT2Tokenizer.from_pretrained(model.model_name)
        model_instance = GPT2LMHeadModel.from_pretrained(model.model_name)

        resp_encode = tokenizer.encode(prompt, return_tensors='pt')

        outputs = model_instance.generate(resp_encode, max_length=model.model_nb_tokens, temperature=model.model_temperature)
        resp = tokenizer.decode(outputs[0], skip_special_tokens=True)
        return resp
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

def post_trait(ch: str):
    ch = str(ch)
    ch = ch.replace("//", " ")
    return ch

app = FastAPI()

@app.post("/chat")
async def API_chat(request:Request):
    data = await request.json()
    prompt=data.get("prompt")
    model = Model(model_name="gpt2", model_nb_tokens=200, model_temperature=0.7)
    
    try:
        resp = await process(prompt, model)
        response = post_trait(str(resp))
        return response
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
