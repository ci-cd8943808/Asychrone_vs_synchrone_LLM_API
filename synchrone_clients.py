import requests
import time

def measure_execution_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Temps d'exécution de {func.__name__}: {end_time - start_time} secondes")
        return result
    return wrapper

def send_request(prompt):
    response = requests.post("http://localhost:8000/chat", json={"prompt": prompt}, timeout=100)
    return response.text

@measure_execution_time
def main():
    prompts = ["bonjour", "çava?", "definir toi  ?"]
    tasks = [send_request(prompt) for prompt in prompts]
    responses = [task for task in tasks]
    for prompt, response in zip(prompts, responses):
        print(f"Prompt: {prompt}, Response: {response}")

if __name__ == "__main__":
    main()
