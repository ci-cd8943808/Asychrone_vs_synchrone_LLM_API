import asyncio
import httpx
import time

def measure_execution_time(func):
    async def wrapper(*args, **kwargs):
        start_time = time.time()
        result = await func(*args, **kwargs)
        end_time = time.time()
        print(f"Temps d'exécution de {func.__name__}: {end_time - start_time} secondes")
        return result
    return wrapper

async def send_request(prompt):
    async with httpx.AsyncClient() as client:
        response = await client.post("http://localhost:8000/chat", json={"prompt": prompt},timeout= 100)
        return response.text
@measure_execution_time
async def main():
    prompts = ["bonjour", "çava?", "definir toi  ?"]
    tasks = [send_request(prompt) for prompt in prompts]
    responses = await asyncio.gather(*tasks)
    for prompt, response in zip(prompts, responses):
        print(f"Prompt: {prompt}, Response: {response}")

if __name__ == "__main__":
    asyncio.run(main())
